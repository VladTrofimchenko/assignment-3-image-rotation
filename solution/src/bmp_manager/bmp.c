#include "bmp.h"
#include "bmp_status.h"
#include "image.h"
#include <inttypes.h>
#include <stdio.h>

enum {
    BMP_PIXEL_SIZE = sizeof(struct pixel),
    BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

uint32_t padding_calc(const uint32_t width) {
    //if (width % 4 == 0) return 0;
    //return (4 - (width * BMP_PIXEL_SIZE % 4));
    return (4 - (width * 3) % 4) % 4;

}


uint32_t image_size_calc(const uint32_t width, const uint32_t height) {
    return (width + padding_calc(width)) * BMP_PIXEL_SIZE * height;
}

uint32_t file_size_calc(const uint32_t img_size) {
    return (img_size + BMP_HEADER_SIZE);
}

/*uint32_t image_size_calculation(const struct image* img) {
   return (img->height) * (BMP_PIXEL_SIZE * (img->width) + padding_calc(img->width));
}*/


/*bool read_header(FILE* const file, struct bmp_header* const header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}*/



enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;
   

    if (fread(&header, BMP_HEADER_SIZE, 1, in) != 1) {
        return BMP_READ_ERROR_IO;
    }

    //if (!read_header(in, &header)) { 
      //  return READ_INVALID_HEADER; 
    //}

    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != PIXEL_BY_BIT_SIZE) {
        return READ_INVALID_BITS;
    }

    if (header.biSize <= 0) {
        return READ_INVALID_FILE_SIZE;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_IMAGE_SIZE;
    }


    //const uint32_t img_width = header.biWidth;
    //const uint32_t img_height = header.biHeight;
    *image = image_create(header.biWidth, header.biHeight);
    
    if (image->data == NULL) {
        return READ_BMP_ERROR_D;
    }

    uint32_t padding = padding_calc((uint32_t)image->width);


    for (size_t i = 0; i < image->height; i++) {
        void *ptr_start = image->data + image->width * i;

        if (fread(ptr_start, BMP_PIXEL_SIZE, image->width, in) != image->width) {
            image_delete(*image);
            return READ_BMP_ERROR_1;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            image_delete(*image);
            return READ_BMP_ERROR_2;
        }

        
        
        
        //fread(ptr_start, sizeof(struct pixel) * header.biWidth, 1, in);
        //fseek(in, padding, SEEK_CUR);

      


    }

    return READ_BMP_OK;

}



enum write_status to_bmp(FILE* out, const struct image* image) {
    struct bmp_header out_header;
    //const uint32_t image_width = (uint32_t)image->width;
    //const uint32_t image_height = (uint32_t)image->height;
    //const uint32_t image_data = image->data;
    uint32_t padding = padding_calc((uint32_t)image->width);
    
    //image->height*(image->width + padding)*BMP_PIXEL_SIZE
    const uint32_t image_size = image_size_calc(image->width, image->height);
    
    
    //BMP_HEADER_SIZE + BMP_PIXEL_SIZE * image->height * (image->width + padding),
    const uint32_t file_size = file_size_calc(image_size);
    

    out_header = (struct bmp_header){
        .bfType = BMP_SIGNATURE,
        .bfileSize = file_size,
        .bfReserved = 0, 
        .bOffBits = BMP_HEADER_SIZE,
        .biSize = BMP_BYTE_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = PIXEL_BY_BIT_SIZE,
        .biCompression = COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = NOTUSED,
        .biYPelsPerMeter = NOTUSED,
        .biClrUsed = NOTUSED,
        .biClrImportant = NOTUSED
    };
    
    if (fwrite(&out_header, BMP_HEADER_SIZE, 1, out) == 0) {
        return WRITE_BMP_ERROR;
    }



    for (size_t i = 0; i < image->height; i++) {
        void* ptr_start = (image->data + image->width * i);

        if (fwrite(ptr_start, BMP_PIXEL_SIZE, image->width, out) == 0) {
            image_delete(*image);
            return WRITE_BMP_ERROR;
        }
        uint8_t garb[3] = {0};

        if ((padding != 0) && (!fwrite(&garb, padding, 1, out))) {  //Write garbage bytes
            image_delete(*image);
            return WRITE_BMP_ERROR;
        }


        /*else {
            for (uint8_t j = 0; j < padding; j++) {
                fwrite(&zero, 1, 1, out);
            }

        }*/
    }
    return WRITE_BMP_OK;



    
}
