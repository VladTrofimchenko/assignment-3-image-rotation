#include "transform.h"
#include "image.h"
#include <stdint.h>

struct image rotate_img(struct image const source) {
  struct image r_image = image_create(source.height, source.width);

  /*if (r_image.data == NULL) {
      return r_image;
  }*/

  // new pixel (x, y) == (y, height - 1 - x)

  for (size_t i = 0; i < r_image.height; i++) {
    for (size_t j = 0; j < r_image.width; j++) {
      r_image.data[i * r_image.width + j] =
          source.data[(source.height - j - 1) * source.width + i];
    }
  }
  return r_image;
}
