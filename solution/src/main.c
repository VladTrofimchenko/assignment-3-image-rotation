#include "main.h"
#include "bmp.h"
#include "bmp_status.h"
#include "file.h"
#include "image.h"
#include "print_errors.h"
#include "transform.h"
#include <stdio.h>

#include <stdlib.h>

void free_f(FILE **f1, FILE **f2) {
  free(f1);
  free(f2);
}

int main(int argc, char **argv) {
  struct image old_img;
  struct image rotated_image;
  enum state open_file_status;
  enum state close_file_status;

  if (argc != 3) {
    printf(PASS_ARG_ERR);
    return 1;
  } 
 

  FILE **file_in = malloc(sizeof(FILE *));
  FILE **file_out = malloc(sizeof(FILE *));

  open_file_status = open_f(file_in, argv[1], "rb");
  if (open_file_status == SUCCESS) {
    printf(INPUT_FILE_OPEN_SUCCESS);
  } else {
    printf(INPUT_FILE_OPEN_ERR);
    free_f(file_in, file_out);
    return -1;
  }

  open_file_status = open_f(file_out, argv[2], "wb");
  if (open_file_status == SUCCESS) {
    printf(OUTPUT_FILE_OPEN_SUCCESS);
  } else {
    printf(OUTPUT_FILE_OPEN_ERR);
    free_f(file_in, file_out);
    return -1;
  }

  //struct image *old_img = malloc(sizeof(struct image));

  enum read_status f_read_status = from_bmp(*file_in, &old_img);
  switch (f_read_status) {
  case BMP_READ_ERROR_IO:
    printf(READ_BMP_ERR);
    // image_delete(*old_img);
    break;
  case READ_BMP_ERROR_D:
    printf(READ_BMP_ERR_DATA);
    // image_delete(*old_img);
    break;
  case READ_INVALID_BITS:
    printf(READ_INVALID_BITS_ERR);
    // image_delete(*old_img);
    break;
  case READ_BMP_ERROR_1:
    printf(READ_BMP_ERR_FOR_DEBUG_1);
    // image_delete(*old_img);
    // printf(len);
    break;
  case READ_BMP_ERROR_2:
    printf(READ_BMP_ERR_FOR_DEBUG_2);
    // image_delete(*old_img);
    break;
  case READ_INVALID_FILE_SIZE:
    printf(READ_INVALID_FILE_SIZE_ERR);
    // image_delete(*old_img);
    break;
  case READ_INVALID_SIGNATURE:
    printf(READ_INVALID_SIGNATURE_ERR);
    // image_delete(*old_img);
    break;
  case READ_INVALID_IMAGE_SIZE:
    printf(READ_INVALID_IMAGE_SIZE_ERR);
    // image_delete(*old_img);
    break;
  case READ_INVALID_HEADER:
    printf(READ_INVALID_HEADER_ERR);
    // image_delete(*old_img);
    break;
  default:
    break;
  }

  if (f_read_status != READ_BMP_OK) {
    free_f(file_in, file_out);
    return -1;
  }

  //struct image *rotated_image = malloc(sizeof(struct image));
  rotated_image = rotate_img(old_img);
  printf(IMAGE_ROTATE_SUCCESS);
  
  /* if ((*rotated_image).data == NULL) {
    printf("image data = null");
    image_delete(*rotated_image);
    free_f(file_in, file_out);
    return -1;
  }*/

  image_delete(old_img);

  enum write_status f_write_status = to_bmp(*file_out, &rotated_image);
  if (f_write_status == WRITE_BMP_OK) {
    printf(BMP_WRITE_SUCCESS);
  } 
  else {
    image_delete(rotated_image);
    free_f(file_in, file_out);
    printf(BMP_WRITE_ERR);
    return -1;
  }

  image_delete(rotated_image);

  close_file_status = close_f(*file_in);
  if (close_file_status == SUCCESS) {
    printf(INPUT_FILE_CLOSE_SUCCESS);
  }

  else {
    printf(INPUT_FILE_CLOSE_ERR);
    free_f(file_in, file_out);
    return -1;
  }

  close_file_status = close_f(*file_out);

  if (close_file_status == SUCCESS) {
    printf(OUTPUT_FILE_CLOSE_SUCCESS);
  }

  else {
    printf(OUTPUT_FILE_CLOSE_ERR);
    free_f(file_in, file_out);
    return -1;
  }

  // free_f(file_in, file_out);
  
  
  free_f(file_in, file_out);

  return 0;
}
