#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H
#define BMP_SIGNATURE 0x4d42
#define PIXEL_BY_BIT_SIZE 24
#define BMP_BYTE_SIZE 40
#define COMPRESSION 0
#define NOTUSED 0

#include "bmp_status.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *image);
enum write_status to_bmp(FILE *out, const struct image *image);
uint32_t image_size_calc(const uint32_t width, const uint32_t height);
uint32_t file_size_calc(const uint32_t img_size);
uint32_t padding_calc(const uint32_t width);

// size_t file_size_calc(size_t img_size);
// bool read_header(FILE* const file, struct bmp_header* const header);

#endif // !IMAGE_ROTATION_BMP_H
