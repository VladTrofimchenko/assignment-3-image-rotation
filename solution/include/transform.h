#ifndef IMAGE_ROTATION_TRANSFORM_H
#define IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotate_img(struct image const source);

#endif // !IMAGE_ROTATION_TRANSFORM_H
