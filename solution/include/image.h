#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
  uint8_t b, g, r;
};
#pragma pack(pop)

#pragma pack(push, 1)

struct image {
  uint32_t width;
  uint32_t height;
  struct pixel *data;
};
#pragma pack(pop)

struct image image_create(const uint32_t width, const uint32_t height);

void image_delete(struct image img);

#endif
